#ifndef POT8_OPCODES_IO_H
#define POT8_OPCODES_IO_H

#include <stdint.h>

#include "pot8_defs.h"
#include "pot8_instance.h"

void pot8_output(pot8_instance_t *instance, uint16_t *p, pot8_op_t op, bool high);

#endif //POT8_OPCODES_IO_H
