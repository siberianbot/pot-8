#ifndef POT8_OPCODES_JMP_H
#define POT8_OPCODES_JMP_H

#include <stdint.h>

#include "pot8_defs.h"
#include "pot8_instance.h"

void pot8_jump(pot8_instance_t *instance, uint16_t *p, pot8_op_t op);

#endif //POT8_OPCODES_JMP_H
