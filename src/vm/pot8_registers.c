#include "pot8_registers.h"

uint8_t pot8_register_get_low(pot8_register_t reg)
{
    return reg.bytes[0];
}

void pot8_register_set_low(pot8_register_t *reg, uint8_t value)
{
    reg->bytes[0] = value;
}

uint8_t pot8_register_get_high(pot8_register_t reg)
{
    return reg.bytes[1];
}

void pot8_register_set_high(pot8_register_t *reg, uint8_t value)
{
    reg->bytes[1] = value;
}

bool pot8_register_get_flag(pot8_registers_t *regs, uint8_t flag)
{
    return regs->f.value & flag;
}

void pot8_register_set_flag(pot8_registers_t *regs, uint8_t flag, bool value)
{
    if (value)
    {
        regs->f.value |= flag;
    }
    else
    {
        regs->f.value &= ~flag;
    }
}

void pot8_register_reset_flags(pot8_registers_t *regs)
{
    regs->f.value = 0;
}
