#ifndef POT8_OPCODES_H
#define POT8_OPCODES_H

#include <stdbool.h>
#include <stdint.h>

#include "pot8_defs.h"
#include "pot8_instance.h"

bool pot8_execute_operand(pot8_instance_t *instance, uint16_t *p, pot8_op_t op);

#endif //POT8_OPCODES_H
