#include "pot8_opcodes_arithmetic.h"

#include <stdio.h>
#include <signal.h>

#include "pot8_utils.h"

void pot8_increment(pot8_instance_t *instance, uint16_t *p, pot8_op_t op)
{
    uint8_t reg = op & 0xF;
    uint8_t byte;

    switch (reg)
    {
        case 1: // Al
            byte = pot8_register_get_low(instance->registers.a);
            pot8_register_set_low(&instance->registers.a, ++byte);
            pot8_handle_value(instance, byte);
            break;

        case 2: // Ah
            byte = pot8_register_get_high(instance->registers.a);
            pot8_register_set_high(&instance->registers.a, ++byte);
            pot8_handle_value(instance, byte);
            break;

        case 3: // Bl
            byte = pot8_register_get_low(instance->registers.b);
            pot8_register_set_low(&instance->registers.b, ++byte);
            pot8_handle_value(instance, byte);
            break;

        case 4: // Bh
            byte = pot8_register_get_high(instance->registers.b);
            pot8_register_set_high(&instance->registers.b, ++byte);
            pot8_handle_value(instance, byte);
            break;

        case 5: // Cl
            byte = pot8_register_get_low(instance->registers.c);
            pot8_register_set_low(&instance->registers.c, ++byte);
            pot8_handle_value(instance, byte);
            break;

        case 6: // Ch
            byte = pot8_register_get_high(instance->registers.c);
            pot8_register_set_high(&instance->registers.c, ++byte);
            pot8_handle_value(instance, byte);
            break;

        case 7: // Dl
            byte = pot8_register_get_low(instance->registers.c);
            pot8_register_set_low(&instance->registers.c, ++byte);
            pot8_handle_value(instance, byte);
            break;

        case 8: // Dh
            byte = pot8_register_get_high(instance->registers.d);
            pot8_register_set_high(&instance->registers.d, ++byte);
            pot8_handle_value(instance, byte);
            break;

        case 9: // Fl
            byte = pot8_register_get_low(instance->registers.f);
            pot8_register_set_low(&instance->registers.f, ++byte);
            pot8_handle_value(instance, byte);
            break;

        case 10: // A
            instance->registers.a.value++;
            pot8_handle_value(instance, instance->registers.a.value);
            break;

        case 11: // B
            instance->registers.b.value++;
            pot8_handle_value(instance, instance->registers.b.value);
            break;

        case 12: // C
            instance->registers.c.value++;
            pot8_handle_value(instance, instance->registers.c.value);
            break;

        case 13: // D
            instance->registers.d.value++;
            pot8_handle_value(instance, instance->registers.d.value);
            break;

        case 14: // P
            instance->registers.p.value++;
            pot8_handle_value(instance, instance->registers.p.value);
            break;

        case 15: // S
            instance->registers.s.value++;
            pot8_handle_value(instance, instance->registers.s.value);
            break;

        default:
            printf("VM:ERR:OP_INC (@0x%x): proc called with invalid argument\n", *p);
            raise(SIGILL);
            break;
    }
}

void pot8_decrement(pot8_instance_t *instance, uint16_t *p, pot8_op_t op)
{
    uint8_t reg = op & 0xF;
    uint8_t byte;

    switch (reg)
    {
        case 1: // Al
            byte = pot8_register_get_low(instance->registers.a);
            pot8_register_set_low(&instance->registers.a, --byte);
            pot8_handle_value(instance, byte);
            break;

        case 2: // Ah
            byte = pot8_register_get_high(instance->registers.a);
            pot8_register_set_high(&instance->registers.a, --byte);
            pot8_handle_value(instance, byte);
            break;

        case 3: // Bl
            byte = pot8_register_get_low(instance->registers.b);
            pot8_register_set_low(&instance->registers.b, --byte);
            pot8_handle_value(instance, byte);
            break;

        case 4: // Bh
            byte = pot8_register_get_high(instance->registers.b);
            pot8_register_set_high(&instance->registers.b, --byte);
            pot8_handle_value(instance, byte);
            break;

        case 5: // Cl
            byte = pot8_register_get_low(instance->registers.c);
            pot8_register_set_low(&instance->registers.c, --byte);
            pot8_handle_value(instance, byte);
            break;

        case 6: // Ch
            byte = pot8_register_get_high(instance->registers.c);
            pot8_register_set_high(&instance->registers.c, --byte);
            pot8_handle_value(instance, byte);
            break;

        case 7: // Dl
            byte = pot8_register_get_low(instance->registers.c);
            pot8_register_set_low(&instance->registers.c, --byte);
            pot8_handle_value(instance, byte);
            break;

        case 8: // Dh
            byte = pot8_register_get_high(instance->registers.d);
            pot8_register_set_high(&instance->registers.d, --byte);
            pot8_handle_value(instance, byte);
            break;

        case 9: // Fl
            byte = pot8_register_get_low(instance->registers.f);
            pot8_register_set_low(&instance->registers.f, --byte);
            pot8_handle_value(instance, byte);
            break;

        case 10: // A
            instance->registers.a.value--;
            pot8_handle_value(instance, instance->registers.a.value);

        case 11: // B
            instance->registers.b.value--;
            pot8_handle_value(instance, instance->registers.b.value);

        case 12: // C
            instance->registers.c.value--;
            pot8_handle_value(instance, instance->registers.c.value);

        case 13: // D
            instance->registers.d.value--;
            pot8_handle_value(instance, instance->registers.d.value);

        case 14: // P
            instance->registers.p.value--;
            pot8_handle_value(instance, instance->registers.p.value);

        case 15: // S
            instance->registers.s.value--;
            pot8_handle_value(instance, instance->registers.s.value);

        default:
            printf("VM:ERR:OP_DCR (@0x%x): proc called with invalid argument\n", *p);
            raise(SIGILL);
            break;
    }
}
