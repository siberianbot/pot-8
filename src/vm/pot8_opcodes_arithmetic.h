#ifndef POT8_OPCODES_ARITHMETIC_H
#define POT8_OPCODES_ARITHMETIC_H

#include <stdint.h>

#include "pot8_defs.h"
#include "pot8_instance.h"

void pot8_increment(pot8_instance_t *instance, uint16_t *p, pot8_op_t op);
void pot8_decrement(pot8_instance_t *instance, uint16_t *p, pot8_op_t op);

#endif //POT8_OPCODES_ARITHMETIC_H
