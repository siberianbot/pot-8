#ifndef POT8_REGISTERS_H
#define POT8_REGISTERS_H

#include <stdbool.h>
#include <stdint.h>

union pot8_register
{
    uint8_t bytes[2];
    uint16_t value;
};

typedef union pot8_register pot8_register_t;

struct pot8_registers
{
    pot8_register_t a;
    pot8_register_t b;
    pot8_register_t c;
    pot8_register_t d;
    pot8_register_t f;
    pot8_register_t p;
    pot8_register_t s;
};

typedef struct pot8_registers pot8_registers_t;

uint8_t pot8_register_get_low(pot8_register_t reg);
uint8_t pot8_register_get_high(pot8_register_t reg);
void pot8_register_set_low(pot8_register_t *reg, uint8_t value);
void pot8_register_set_high(pot8_register_t *reg, uint8_t value);

bool pot8_register_get_flag(pot8_registers_t *regs, uint8_t flag);
void pot8_register_set_flag(pot8_registers_t *regs, uint8_t flag, bool value);
void pot8_register_reset_flags(pot8_registers_t *regs);

#endif //POT8_REGISTERS_H
