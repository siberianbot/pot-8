#include <stdio.h>
#include <stdlib.h>
#include <signal.h>

#include "pot8_instance.h"
#include "pot8_opcodes.h"

void pot8_execute(pot8_instance_t *instance)
{
    bool halted = false;

    while (!halted)
    {
        uint16_t p = instance->registers.p.value;

        halted = pot8_execute_operand(instance, &p, instance->mem[p]);

        if (p >= instance->size)
        {
            p = p - instance->size;
        }

        instance->registers.p.value = p;
    }
}

static void error_handler(int signo)
{
    printf("VM failure.\n");

    abort();
}

int main(int argc, char **argv)
{
    signal(SIGILL, error_handler);

    printf("POT-8 VM\n");

    if (argc < 2)
    {
        printf("usage: ./pot8vm <image file>\n");
        return 0;
    }

    pot8_instance_t instance;

    pot8_reset(&instance);

    if (!pot8_read_file(argv[1], &instance))
    {
        printf("invalid image file\n");
        return -1;
    }

    pot8_execute(&instance);

    return 0;
}