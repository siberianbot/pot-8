#ifndef POT8_UTILS_H
#define POT8_UTILS_H

#include <stdbool.h>
#include <stdint.h>

#include "pot8_instance.h"

void pot8_handle_value(pot8_instance_t *instance, uint16_t value);
void pot8_set_when(bool predicate, uint16_t *p, uint16_t addr);

#endif //POT8_UTILS_H
