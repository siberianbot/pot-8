#include "pot8_opcodes_jmp.h"

#include <stdio.h>
#include <signal.h>

#include "pot8_utils.h"

void pot8_jump(pot8_instance_t *instance, uint16_t *p, pot8_op_t op)
{
    uint16_t initial_p = *p;

    uint16_t kind = op & 0xF;
    uint16_t addr = instance->mem[++(*p)];
    addr |= ((uint16_t) instance->mem[++(*p)]) << 8;

    if (addr >= instance->size)
    {
        printf("VM:ERR:OP_JMP (@0x%x): invalid addr 0x%x\n", initial_p, addr);
        raise(SIGILL);
        return;
    }

    switch (kind)
    {
        case 0: // JMP
            *p = addr;
            break;

        case 1: // JNS
            pot8_set_when(!pot8_register_get_flag(&instance->registers, POT8_FLAGS_SIGN), p, addr);
            break;

        case 2: // JS
            pot8_set_when(pot8_register_get_flag(&instance->registers, POT8_FLAGS_SIGN), p, addr);
            break;

        case 3: // JNP
            pot8_set_when(!pot8_register_get_flag(&instance->registers, POT8_FLAGS_PARITY), p, addr);
            break;

        case 4: // JP
            pot8_set_when(pot8_register_get_flag(&instance->registers, POT8_FLAGS_PARITY), p, addr);
            break;

        case 5: // JNZ
            pot8_set_when(!pot8_register_get_flag(&instance->registers, POT8_FLAGS_ZERO), p, addr);
            break;

        case 6: // JZ
            pot8_set_when(pot8_register_get_flag(&instance->registers, POT8_FLAGS_ZERO), p, addr);
            break;

        case 7: // JNC
            pot8_set_when(!pot8_register_get_flag(&instance->registers, POT8_FLAGS_CARRY), p, addr);
            break;

        case 8: // JC
            pot8_set_when(pot8_register_get_flag(&instance->registers, POT8_FLAGS_CARRY), p, addr);
            break;

        case 9: // JNI
            pot8_set_when(!pot8_register_get_flag(&instance->registers, POT8_FLAGS_ILLEGAL), p, addr);
            break;

        case 10: // JI
            pot8_set_when(pot8_register_get_flag(&instance->registers, POT8_FLAGS_ILLEGAL), p, addr);
            break;

        default:
            printf("VM:ERR:OP_JMP (@0x%x): proc called with invalid argument\n", initial_p);
            raise(SIGILL);
            break;
    }
}
