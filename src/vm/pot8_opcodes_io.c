#include "pot8_opcodes_io.h"

#include <stdio.h>

void pot8_output(pot8_instance_t *instance, uint16_t *p, pot8_op_t op, bool high)
{
    uint8_t outputId = instance->mem[++(*p)]; // TODO: not used
    uint8_t value = high ? pot8_register_get_high(instance->registers.a) : pot8_register_get_low(instance->registers.a);

    printf("%c", value);
}