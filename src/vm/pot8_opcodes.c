#include "pot8_opcodes.h"

#include <stdio.h>

#include "pot8_utils.h"

#include "pot8_opcodes_arithmetic.h"
#include "pot8_opcodes_jmp.h"
#include "pot8_opcodes_load.h"
#include "pot8_opcodes_io.h"

bool pot8_execute_operand(pot8_instance_t *instance, uint16_t *p, pot8_op_t op)
{
    pot8_register_set_flag(&instance->registers, POT8_FLAGS_ILLEGAL, false);

    if (op == 0xFF) // HLT
    {
        return true;
    }

    if (op >= 0x01 && op <= 0x09)
    {
        pot8_load_value_immediate_8bit(instance, p, op);
        (*p)++;

        return false;
    }

    if (op >= 0x0A && op <= 0x0F)
    {
        pot8_load_value_immediate_16bit(instance, p, op);
        (*p)++;

        return false;
    }

    if (op >= 0x11 && op <= 0x19)
    {
        pot8_load_value_memory_8bit(instance, p, op);
        (*p)++;

        return false;
    }

    if (op >= 0x11 && op <= 0x1F)
    {
        pot8_load_value_memory_16bit(instance, p, op);
        (*p)++;

        return false;
    }

    if (op >= 0x40 && op <= 0x4A)
    {
        pot8_jump(instance, p, op);
        return false;
    }

    if (op >= 0x51 && op <= 0x5F)
    {
        pot8_increment(instance, p, op);
        (*p)++;

        return false;
    }

    if (op >= 0x61 && op <= 0x6F)
    {
        pot8_decrement(instance, p, op);
        (*p)++;

        return false;
    }

    if (op >= 0x70 && op <= 0x7B)
    {
        pot8_load_value_accumulator(instance, p, op);
        (*p)++;

        return false;
    }

    if (op == 0x50 || op == 0x60)
    {
        pot8_output(instance, p, op, op == 0x50);
        (*p)++;

        return false;
    }

    (*p)++;

    if (op != 0x00) // not NOP
    {
        printf("DEBUG: illegal opcode 0x%x\n", op);
        pot8_register_set_flag(&instance->registers, POT8_FLAGS_ILLEGAL, true);
    }

    return false;
}
