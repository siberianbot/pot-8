#include "pot8_utils.h"

#include "pot8_defs.h"

void pot8_handle_value(pot8_instance_t *instance, uint16_t value)
{
    pot8_register_set_flag(&instance->registers, POT8_FLAGS_ZERO, value == 0);
    pot8_register_set_flag(&instance->registers, POT8_FLAGS_PARITY, (value & 1) == 1);
}

void pot8_set_when(bool predicate, uint16_t *p, uint16_t addr)
{
    if (predicate)
    {
        *p = addr;
    }
}
