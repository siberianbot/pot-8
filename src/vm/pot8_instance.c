#include "pot8_instance.h"

#define _CRT_SECURE_NO_WARNINGS 1

#include <stdio.h>
#include <stdlib.h>

void pot8_reset(pot8_instance_t *instance)
{
    instance->size = 0;
    instance->mem = 0;
    instance->registers.a.value = 0;
    instance->registers.b.value = 0;
    instance->registers.c.value = 0;
    instance->registers.d.value = 0;
    instance->registers.f.value = 0;
    instance->registers.p.value = 0;
    instance->registers.s.value = 0;
}

bool pot8_read_file(const char *path, pot8_instance_t *instance)
{
    long size;
    void *mem;

    if (!path)
    {
        return false;
    }

    FILE *img = fopen(path, "rb");

    if (!img)
    {
        return false;
    }

    fseek(img, 0L, SEEK_END);
    size = ftell(img);

    rewind(img);

    mem = (char *) malloc(size);

    fgets(mem, size, img);
    fclose(img);

    instance->size = size;
    instance->mem = mem;

    return true;
}
