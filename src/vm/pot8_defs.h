#ifndef POT8_DEFS_H
#define POT8_DEFS_H

#include <stdint.h>

#define POT8_FLAGS_SIGN     1 << 0
#define POT8_FLAGS_PARITY   1 << 1
#define POT8_FLAGS_ZERO     1 << 2
#define POT8_FLAGS_CARRY    1 << 3
#define POT8_FLAGS_ILLEGAL  1 << 4

typedef uint8_t pot8_op_t;

#endif //POT8_DEFS_H
