#ifndef POT8_INSTANCE_H
#define POT8_INSTANCE_H

#include <stdbool.h>

#include "pot8_registers.h"

struct pot8_instance
{
    long size;
    uint8_t *mem;

    pot8_registers_t registers;
};

typedef struct pot8_instance pot8_instance_t;

void pot8_reset(pot8_instance_t *instance);
bool pot8_read_file(const char *path, pot8_instance_t *instance);

#endif //POT8_INSTANCE_H
