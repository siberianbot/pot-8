#include "pot8_opcodes_load.h"

#include <stdio.h>
#include <signal.h>

#include "pot8_utils.h"

void pot8_load_value_immediate_8bit(pot8_instance_t *instance, uint16_t *p, pot8_op_t op)
{
    uint16_t initial_p = *p;

    uint16_t reg = op & 0xF;
    uint8_t value = instance->mem[++(*p)];

    pot8_handle_value(instance, value);

    switch (reg)
    {
        case 0b0001: // Al
            pot8_register_set_low(&instance->registers.a, value);
            break;

        case 0b0010: // Ah
            pot8_register_set_high(&instance->registers.a, value);
            break;

        case 0b0011: // Bl
            pot8_register_set_low(&instance->registers.b, value);
            break;

        case 0b0100: // Bh
            pot8_register_set_high(&instance->registers.b, value);
            break;

        case 0b0101: // Cl
            pot8_register_set_low(&instance->registers.c, value);
            break;

        case 0b0110: // Ch
            pot8_register_set_high(&instance->registers.c, value);
            break;

        case 0b0111: // Dl
            pot8_register_set_low(&instance->registers.d, value);
            break;

        case 0b1000: // Dh
            pot8_register_set_high(&instance->registers.d, value);
            break;

        case 0b1001: // Fl
            pot8_register_set_low(&instance->registers.f, value);
            break;

        default:
            printf("VM:ERR:OP_LV8 (@0x%x): proc called for invalid register\n", initial_p);
            raise(SIGILL);
            break;
    }
}

void pot8_load_value_immediate_16bit(pot8_instance_t *instance, uint16_t *p, pot8_op_t op)
{
    uint16_t initial_p = *p;

    uint16_t reg = op & 0xF;
    uint16_t value = instance->mem[++(*p)];
    value |= ((uint16_t) instance->mem[++(*p)]) << 8;

    pot8_handle_value(instance, value);

    switch (reg)
    {
        case 0b1010: // A
            instance->registers.a.value = value;
            break;

        case 0b1011: // B
            instance->registers.b.value = value;
            break;

        case 0b1100: // C
            instance->registers.c.value = value;
            break;

        case 0b1101: // D
            instance->registers.d.value = value;
            break;

        case 0b1110: // P
            instance->registers.p.value = value;
            break;

        case 0b1111: // S
            instance->registers.s.value = value;
            break;

        default:
            printf("VM:ERR:OP_LV16 (@0x%x): proc called for invalid register\n", initial_p);
            raise(SIGILL);
            break;
    }
}

void pot8_load_value_memory_8bit(pot8_instance_t *instance, uint16_t *p, pot8_op_t op)
{
    uint16_t initial_p = *p;

    uint16_t reg = op & 0xF;
    uint16_t addr = instance->mem[++(*p)];
    addr |= ((uint16_t) instance->mem[++(*p)]) << 8;

    if (addr >= instance->size)
    {
        printf("VM:ERR:OP_LVX8 (@0x%x): invalid addr 0x%x\n", initial_p, addr);
        raise(SIGILL);
        return;
    }

    uint8_t value = instance->mem[addr];

    pot8_handle_value(instance, value);

    switch (reg)
    {
        case 0b0001: // Al
            pot8_register_set_low(&instance->registers.a, value);
            break;

        case 0b0010: // Ah
            pot8_register_set_high(&instance->registers.a, value);
            break;

        case 0b0011: // Bl
            pot8_register_set_low(&instance->registers.b, value);
            break;

        case 0b0100: // Bh
            pot8_register_set_high(&instance->registers.b, value);
            break;

        case 0b0101: // Cl
            pot8_register_set_low(&instance->registers.c, value);
            break;

        case 0b0110: // Ch
            pot8_register_set_high(&instance->registers.c, value);
            break;

        case 0b0111: // Dl
            pot8_register_set_low(&instance->registers.d, value);
            break;

        case 0b1000: // Dh
            pot8_register_set_high(&instance->registers.d, value);
            break;

        case 0b1001: // Fl
            pot8_register_set_low(&instance->registers.f, value);
            break;

        default:
            printf("VM:ERR:OP_LVX8 (@0x%x): proc called for invalid register\n", initial_p);
            raise(SIGILL);
            break;
    }
}

void pot8_load_value_memory_16bit(pot8_instance_t *instance, uint16_t *p, pot8_op_t op)
{
    uint16_t initial_p = *p;

    uint16_t reg = op & 0xF;
    uint16_t addr = instance->mem[++(*p)];
    addr |= ((uint16_t) instance->mem[++(*p)]) << 8;

    if (addr >= instance->size)
    {
        printf("VM:ERR:OP_LVX16 (@0x%x): invalid addr 0x%x\n", initial_p, addr);
        raise(SIGILL);
        return;
    }

    uint16_t value = instance->mem[addr];
    value |= ((uint16_t) instance->mem[addr + 1]) << 8;

    pot8_handle_value(instance, value);

    switch (reg)
    {
        case 0b1010: // A
            instance->registers.a.value = value;
            break;

        case 0b1011: // B
            instance->registers.b.value = value;
            break;

        case 0b1100: // C
            instance->registers.c.value = value;
            break;

        case 0b1101: // D
            instance->registers.d.value = value;
            break;

        case 0b1110: // P
            instance->registers.p.value = value;
            break;

        case 0b1111: // S
            instance->registers.s.value = value;
            break;

        default:
            printf("VM:ERR:OP_LVX16 (@0x%x): proc called for invalid register\n", initial_p);
            raise(SIGILL);
            break;
    }
}

void pot8_load_value_accumulator(pot8_instance_t *instance, uint16_t *p, pot8_op_t op)
{
    uint8_t kind = op & 0xF;

    uint16_t addr;
    bool high = false;
    bool low = false;

    switch (kind)
    {
        case 0: // A -> Ah
            addr = instance->registers.a.value;
            high = true;
            break;

        case 1: // B -> Ah
            addr = instance->registers.b.value;
            high = true;
            break;

        case 2: // C -> Ah
            addr = instance->registers.c.value;
            high = true;
            break;

        case 3: // D -> Ah
            addr = instance->registers.d.value;
            high = true;
            break;

        case 4: // A -> Al
            addr = instance->registers.a.value;
            low = true;
            break;

        case 5: // B -> Al
            addr = instance->registers.b.value;
            low = true;
            break;

        case 6: // C -> Al
            addr = instance->registers.c.value;
            low = true;
            break;

        case 7: // D -> Al
            addr = instance->registers.d.value;
            low = true;
            break;

        case 8: // A -> A
            addr = instance->registers.a.value;
            high = true;
            low = true;
            break;

        case 9: // B -> A
            addr = instance->registers.b.value;
            high = true;
            low = true;
            break;

        case 10: // C -> A
            addr = instance->registers.c.value;
            high = true;
            low = true;
            break;

        case 11: // D -> A
            addr = instance->registers.d.value;
            high = true;
            low = true;
            break;

        default:
            printf("VM:ERR:OP_LVR (@0x%x): proc called with invalid kind\n", *p);
            raise(SIGILL);
            return;
    }

    if (addr >= instance->size)
    {
        printf("VM:ERR:OP_LVX16 (@0x%x): invalid addr 0x%x\n", *p, addr);
        raise(SIGILL);
        return;
    }

    uint16_t value = instance->mem[addr];

    if (high && low)
    {
        value |= ((uint16_t) instance->mem[addr + 1]) << 8;

        instance->registers.a.value = addr;
    }
    else if (high)
    {
        pot8_register_set_high(&instance->registers.a, value);
    }
    else
    {
        pot8_register_set_low(&instance->registers.a, value);
    }

    pot8_handle_value(instance, value);
}