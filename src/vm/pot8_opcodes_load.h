#ifndef POT8_OPCODES_LOAD_H
#define POT8_OPCODES_LOAD_H

#include <stdint.h>

#include "pot8_defs.h"
#include "pot8_instance.h"

void pot8_load_value_immediate_8bit(pot8_instance_t *instance, uint16_t *p, pot8_op_t op);
void pot8_load_value_immediate_16bit(pot8_instance_t *instance, uint16_t *p, pot8_op_t op);
void pot8_load_value_memory_8bit(pot8_instance_t *instance, uint16_t *p, pot8_op_t op);
void pot8_load_value_memory_16bit(pot8_instance_t *instance, uint16_t *p, pot8_op_t op);
void pot8_load_value_accumulator(pot8_instance_t *instance, uint16_t *p, pot8_op_t op);

#endif //POT8_OPCODES_LOAD_H
