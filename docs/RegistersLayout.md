Registers Layout
================

POT-8 have next registers:

- 4 16-bit registers: `A`, `B`, `C`, `D`. All of this registers represents pairs of 8-bit registers (named `high` or `low`).

- 8-bit flags register: `Fl`.

- 16-bit program counter register: `P`.
- 16-bit stack pointer register: `S`.

   | h(igh) |  l(ow) |
---|--------|--------|
 A |  8bit  |  8bit  |
 B |  8bit  |  8bit  |
 C |  8bit  |  8bit  |
 D |  8bit  |  8bit  |
---|--------|--------|
 F |        |  8bit  |
---|--------|--------|
 P |      16bit      |
 S |      16bit      |

## Flags (`Fl` register) ##

`Fl` register contains flags, which sets after operand execution.

   |  1  |  2  |  3  |  4  |  5  |  6  |  7  |  8  |
---|-----|-----|-----|-----|-----|-----|-----|-----|
   |  S  |  P  |  Z  |  C  |  I  | UNUSED - UNUSED |

- `S` - sign flag: denotes that register value became negative after operation:
  - `0` - positive
  - `1` - negative

- `P` - parity-check flag: denotes that register value became even or odd after operation
  - `0` - odd
  - `1` - even

- `Z` - zero-check flag: denotes that register value equals zero after operation

- `C` - carry-check flag: denotes overflow

- `I` - illegal opcode flag: denotes illegal opcode